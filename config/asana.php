<?php

return [
    'client' => [
        'id' => env('ASANA_CLIENT_ID', ''),
        'secret' => env('ASANA_CLIENT_SECRET', ''),
        'redirect_uri' => env('ASANA_REDIRECT_URI', ''),
    ],
];
