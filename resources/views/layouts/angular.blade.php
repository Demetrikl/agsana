<!DOCTYPE html>
<html lang="en">
    <head>
        <base href="/angular">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css"
              integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+"
              crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css"
              integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
              crossorigin="anonymous">

        @include('layouts/_shared_head')
    </head>
    <body id="app-layout" class="agsana">
        @include('layouts/_navbar')

        @yield('content')

        <script src="{{ asset('app/polyfills.bundle.js') }}"></script>
        <script src="{{ asset('app/vendor.bundle.js') }}"></script>
        <script src="{{ asset('app/main.bundle.js') }}"></script>

        @include('layouts/_shared_body')
    </body>
</html>
