import {Project} from "../project/project";

export class Section {
    id:number;
    name:string;
    code:string;
    ids:number[];
    projects:Project[];
}