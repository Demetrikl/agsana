import {Component} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import {DragulaService} from "ng2-dragula/ng2-dragula";
import {ProjectService} from "../project/project.service";
import {Section} from "./section";
import {Project} from "../project/project";

@Component({
    selector: 'agsana-board',
    templateUrl: 'board.html'
})
export class BoardComponent {
    showLoading:boolean = false;
    projectIds:number[] = [];
    sections:Section[] = [];
    projects:Project[] = [];
    projectPromises:Promise<Project>[] = [];

    constructor(private router:Router, private route:ActivatedRoute, private dragulaService:DragulaService, private projectService:ProjectService) {
        dragulaService.setOptions('tasks-board', {
            revertOnSpill: true
        });
    }

    ngOnInit():void {
        this.showLoading = true;

        this.route.queryParams.subscribe(params => {
            this.projectIds = params['projects'].split(',');
        });

        this.projectIds.forEach(projectId => {
            this.projectPromises.push(this.projectService.getProject(projectId));
        });

        Promise.all(this.projectPromises).then(projects => {
            this.projects = projects;

            projects.forEach(project => {
                project.relationSectionTask = {};

                project.sections.forEach(section => {
                    let findedSection = this.sections.find(element => element.name == section.name);

                    if (findedSection) {
                        findedSection.ids.push(section.id);
                        findedSection.projects.push(project);
                    } else {
                        section.code = 'c' + section.id;
                        section.projects = [];
                        section.ids = [];
                        section.ids.push(section.id);
                        section.projects.push(project);
                        this.sections.push(section);
                    }
                });

                project.tasks.forEach(task => {
                    task.memberships.forEach(membership=> {
                        if (membership.hasOwnProperty('section') && membership['project']['id'] == project.id) {

                            let globalSection = this.sections.find(element => element.ids.includes(membership['section']['id']))

                            if (!project.relationSectionTask.hasOwnProperty(globalSection.code)) {
                                project.relationSectionTask[globalSection.code] = [];
                            }

                            project.relationSectionTask[globalSection.code].push(task);
                        }
                    });
                });
            });

            this.showLoading = false;
        }, reason => {
            this.showLoading = false;

            console.log(reason);
        });
    }
}
