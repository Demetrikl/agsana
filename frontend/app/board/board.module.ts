import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {BoardComponent} from "./board.component";
import {DragulaModule} from "ng2-dragula/ng2-dragula";

@NgModule({
    imports: [
        BrowserModule,
        DragulaModule
    ],
    declarations: [BoardComponent],
    bootstrap: [BoardComponent],
})
export class BoardModule {
}