import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {WorkspaceListComponent} from "./workspace/list/workspace-list.component";
import {WorkspaceDetailsComponent} from "./workspace/details/workspace-details.component";
import {BoardComponent} from "./board/board.component";

const routes:Routes = [
    {path: '', redirectTo: '/workspaces', pathMatch: 'full'},
    {path: 'workspaces', component: WorkspaceListComponent},
    {path: 'workspace/:id', component: WorkspaceDetailsComponent},
    {path: 'board', component: BoardComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}