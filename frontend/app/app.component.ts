import {Component} from "@angular/core";

@Component({
    selector: 'agsana-app',
    template: '<router-outlet></router-outlet>',
    styleUrls: ['app.css']
})
export class AppComponent {
}