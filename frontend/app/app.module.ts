import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppRoutingModule} from "./app-routing.module";
import {BoardModule} from "./board/board.module";
import {WorkspaceModule} from "./workspace/workspace.module";
import {ProjectModule} from "./project/project.module";
import {TaskModule} from "./task/task.module";

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        BoardModule,
        WorkspaceModule,
        ProjectModule,
        TaskModule
    ],
    declarations: [AppComponent],
    bootstrap: [AppComponent],
})
export class AppModule {
}