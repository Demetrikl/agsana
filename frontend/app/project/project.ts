import {Section} from "../board/section";
import {Task} from "../task/task";

export class Project {
    id:number;
    name:string;
    sections:Section[];
    tasks:Task[];
    relationSectionTask:Object;
}