import {Workspace} from "../workspace/workspace";

export class User {
    id:number;
    name:string;
    email:string;
    photo:string[];
    workspaces:Workspace[]
}