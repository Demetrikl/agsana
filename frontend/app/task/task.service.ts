import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import "rxjs/add/operator/toPromise";
import {AppSettings} from "../app-settings";
import {Task} from "./task";

@Injectable()
export class TaskService {
    private headers = new Headers({'Content-Type': 'application/json'});
    private tasksUrl = `${AppSettings.API_ENDPOINT}/tasks`;  // URL to web api

    constructor(private http:Http) {
    }

    getTasks():Promise<Task[]> {
        return this.http.get(this.tasksUrl)
            .toPromise()
            .then(response => response.json() as Task[])
            .catch(this.handleError);
    }

    getTask(id:number):Promise<Task> {
        const url = `${this.tasksUrl}/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json() as Task)
            .catch(this.handleError);
    }

    delete(id:number):Promise<void> {
        const url = `${this.tasksUrl}/${id}`;

        return this.http.delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    create(name:string):Promise<Task> {
        return this.http
            .post(this.tasksUrl, JSON.stringify({name: name}), {headers: this.headers})
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    update(task:Task):Promise<Task> {
        const url = `${this.tasksUrl}/${task.id}`;

        return this.http
            .put(url, JSON.stringify(task), {headers: this.headers})
            .toPromise()
            .then(() => task)
            .catch(this.handleError);
    }

    private handleError(error:any):Promise<any> {
        console.error('An error occurred', error); // for demo purposes only

        return Promise.reject(error.message || error);
    }
}