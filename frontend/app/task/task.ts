export class Task {
    id:number;
    name:string;
    memberships:Object[];
    tags:Object[];
    assignee:Object;
}