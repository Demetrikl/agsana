import {NgModule} from "@angular/core";
import {WorkspaceService} from "./workspace.service";
import {BrowserModule} from "@angular/platform-browser";
import {WorkspaceListComponent} from "./list/workspace-list.component";
import {WorkspaceDetailsComponent} from "./details/workspace-details.component";

@NgModule({
    imports: [BrowserModule],
    providers: [WorkspaceService],
    declarations: [
        WorkspaceListComponent,
        WorkspaceDetailsComponent
    ],
    bootstrap: [
        WorkspaceListComponent,
        WorkspaceDetailsComponent
    ]
})
export class WorkspaceModule {
}