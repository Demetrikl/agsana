import {Component} from "@angular/core";
import {WorkspaceService} from "../workspace.service";
import {Workspace} from "../workspace";
import {Router} from "@angular/router";
import {User} from "../../user/user";

@Component({
    selector: 'agsana-board',
    templateUrl: 'workspace-list.html'
})
export class WorkspaceListComponent {
    showLoading:boolean = false;
    workspaces:Workspace[];
    user:User;

    constructor(private workspaceService:WorkspaceService, private router:Router) {
        this.user = new User();
        this.user.photo = [];
    }

    getUser():void {
        this.showLoading = true;
        this.workspaceService.getUser().then(user => {
                this.showLoading = false;
                this.user = user;
                this.workspaces = user.workspaces;
            },
            error => {
                this.showLoading = false;
            });
    }

    getWorkspaces():void {
        this.workspaceService.getWorkspaces().then(workspaces => this.workspaces = workspaces);
    }

    onSelect(workspace:Workspace):void {
        this.router.navigate(['/workspace', workspace.id]);
    }

    ngOnInit():void {
        this.getUser();
    }
}
