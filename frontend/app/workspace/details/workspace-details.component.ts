import {Component} from "@angular/core";
import {Params, ActivatedRoute, Router} from "@angular/router";
import {ProjectService} from "../../project/project.service";
import {Project} from "../../project/project";

@Component({
    selector: 'agsana-board',
    templateUrl: 'workspace-details.html'
})
export class WorkspaceDetailsComponent {
    showLoading:boolean = false;
    showEmptySelectionWarning:boolean = false;

    projects:Project[] = [];
    filteredProjects:Project[] = [];
    selectedProjects:Project[] = [];

    constructor(private projectService:ProjectService, private router:Router, private route:ActivatedRoute) {
    }

    getProjectsByWorkspaceId(id:number):void {
        this.showLoading = true;
        this.projectService.getProjectsByWorkspaceId(id)
            .then(projects => {
                    this.showLoading = false;
                    this.projects = projects;
                    this.filteredProjects = projects;
                },
                error => {
                    this.showLoading = false;
                    this.showEmptySelectionWarning = true;
                });
    }

    projectFilterOnKeyUp(event:any):void {
        this.filteredProjects = this.projects.filter((project) => {
            return project.name.toLowerCase().indexOf(event.target.value.toLowerCase()) !== -1;
        });
    }

    mergeWithList(project:Project):void {
        const index = this.selectedProjects.indexOf(project);

        if (index === -1) {
            this.selectedProjects.push(project);
        } else {
            delete this.selectedProjects[index];
        }
    }

    isSelected(project:Project):boolean {
        return this.selectedProjects.indexOf(project) != -1;
    }

    goBack():void {
        this.router.navigate(['/workspaces']);
    }

    goToBoard():void {
        if (this.selectedProjects.length <= 0) {
            this.showEmptySelectionWarning = true;

            return;
        }

        this.router.navigate(['/board'], {
            queryParams: {
                projects: this.selectedProjects.map((project) => {
                    return project.id;
                })
            }
        });
    }

    ngOnInit():void {
        this.route.params.forEach((params:Params) => {
            let id = +params['id'];

            this.getProjectsByWorkspaceId(id);
        });
    }
}
