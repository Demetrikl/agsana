import {Injectable} from "@angular/core";
import {Http, Headers} from "@angular/http";
import {Workspace} from "./workspace";
import "rxjs/add/operator/toPromise";
import {AppSettings} from "../app-settings";
import {User} from "../user/user";

@Injectable()
export class WorkspaceService {
    private headers = new Headers({'Content-Type': 'application/json'});
    private workspacesUrl = `${AppSettings.API_ENDPOINT}/workspaces`;  // URL to web api
    private userUrl = `${AppSettings.API_ENDPOINT}/user`;

    constructor(private http:Http) {
    }

    getWorkspaces():Promise<Workspace[]> {
        return this.http.get(this.userUrl)
            .toPromise()
            .then(response => response.json() as Workspace[])
            .catch(this.handleError);
    }

    getUser():Promise<User> {
        return this.http.get(this.userUrl)
            .toPromise()
            .then(response => response.json() as User)
            .catch(this.handleError);
    }

    getWorkspace(id:number):Promise<Workspace> {
        const url = `${this.workspacesUrl}/${id}`;

        return this.http.get(url)
            .toPromise()
            .then(response => response.json() as Workspace)
            .catch(this.handleError);
    }

    delete(id:number):Promise<void> {
        const url = `${this.workspacesUrl}/${id}`;

        return this.http.delete(url, {headers: this.headers})
            .toPromise()
            .then(() => null)
            .catch(this.handleError);
    }

    create(name:string):Promise<Workspace> {
        return this.http
            .post(this.workspacesUrl, JSON.stringify({name: name}), {headers: this.headers})
            .toPromise()
            .then(res => res.json().data)
            .catch(this.handleError);
    }

    update(workspace:Workspace):Promise<Workspace> {
        const url = `${this.workspacesUrl}/${workspace.id}`;

        return this.http
            .put(url, JSON.stringify(workspace), {headers: this.headers})
            .toPromise()
            .then(() => workspace)
            .catch(this.handleError);
    }

    private handleError(error:any):Promise<any> {
        console.error('An error occurred', error); // for demo purposes only

        return Promise.reject(error.message || error);
    }
}