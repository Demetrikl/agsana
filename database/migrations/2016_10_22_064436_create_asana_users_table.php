<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAsanaUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asana_users', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedBigInteger('asana_id');
            $table->string('token');
            $table->string('refresh_token');
            $table->dateTime('expire_at');

            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('asana_user_id')->references('id')->on('asana_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_asana_user_id_foreign');
        });

        Schema::drop('asana_users');
    }
}
