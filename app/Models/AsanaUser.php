<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AsanaUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['asana_id', 'token', 'refresh_token', 'expire_at', 'photo'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }
}
