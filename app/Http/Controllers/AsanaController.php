<?php
namespace App\Http\Controllers;

use App\Http\Requests\Request;
use Asana\Client;

class AsanaController extends Controller
{
    /** @var Client */
    private $_client;

    public function __construct(Client $client)
    {
        $this->_client = $client;
    }

    public function user()
    {
        return (array)$this->_client->users->me();
    }

    public function workspaces()
    {
        $result     = [];
        $workspaces = $this->_client->workspaces->findAll();

        foreach ($workspaces as $workspace) {
            $result[] = $workspace;
        }

        return $result;
    }

    public function projects($workspaceId)
    {
        $result   = [];
        $projects = $this->_client->projects->findAll(
            ['workspace' => $workspaceId, 'archived' => false],
            [
                'fields' => ['team', 'name', 'team.id', 'team.name',],
            ]);

        foreach ($projects as $project) {
            $result[] = $project;
        }

        return $result;
    }

    public function projectsSearch($workspaceId, $search)
    {
        $result   = [];
        $projects = $this->_client->workspaces->typeahead($workspaceId, [
            'type'  => 'project',
            'query' => $search,
        ], [
            'fields' => ['team', 'name', 'team.id', 'team.name',],
        ]);

        foreach ($projects as $project) {
            $result[] = $project;
        }

        return $result;
    }

    public function fullProject($projectId)
    {
        $project = (array)$this->_client->projects->findById($projectId);

        $project['sections'] = $this->sections($projectId);
        $project['tasks']    = $this->tasks($projectId);

        return $project;
    }

    public function sections($projectId)
    {
        $result = [];

        $sections = $this->_client->projects->sections($projectId);

        foreach ($sections as $section) {
            array_unshift($result, $section);
        }

        return $result;
    }

    public function tasks($projectId)
    {
        $result = [];

        $tasks = $this->_client->tasks->findAll(['project' => $projectId], [
            'fields' => [
                'completed',
                'name',
                'assignee.id',
                'assignee.name',
                'assignee.photo.image_60x60',
                'memberships.section',
                'memberships.project',
                'tags.color',
                'tags.name',
            ],
        ]);

        foreach ($tasks as $task) {
            if (!$task->completed && strpos($task->name, ':') != (strlen($task->name) - 1)) {
                $result[] = $task;
            }
        }

        return $result;
    }

    public function getTask($taskId)
    {
        return (array)$this->_client->tasks->findById($taskId);
    }

    public function updateTask(Request $request, $taskID)
    {
        return $this->_client->tasks->update($taskID, $request->input());
    }
}