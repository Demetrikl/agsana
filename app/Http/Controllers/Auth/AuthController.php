<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\AuthController\OauthRequest;
use App\Models\AsanaUser;
use App\Models\User;
use Asana\Client;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => ['logout', 'oauth']]);
    }

    /**
     * @param Client $client
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function login(Client $client)
    {
        return response()->json(['url' => $client->dispatcher->authorizationUrl()]);
    }

    /**
     * @param OauthRequest $request
     * @param Client       $client
     *
     * @return \Illuminate\Http\JsonRegsponse
     * @throws \HttpException
     */
    public function oauth(OauthRequest $request, Client $client)
    {
        $token = $client->dispatcher->fetchToken($request->input('code'));

        $user  = $this->create((array)$client->users->me(), [
            'token'        => $token,
            'refreshToken' => $client->dispatcher->refreshToken,
            'expireAt'     => $client->dispatcher->expiresIn,
        ]);

        return response()->json(['token' => $user->api_token]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array|\stdClass $asanaUserData
     *
     * @param array           $authInformation
     *
     * @return User
     * @throws \Throwable
     * @throws \Exception
     */
    protected function create(array $asanaUserData, array $authInformation)
    {
        $asanaUser = AsanaUser::where('asana_id', Arr::get($asanaUserData, 'id'))->first();

        if (!$asanaUser) {
            \DB::transaction(function () use ($asanaUserData, $authInformation, &$asanaUser) {
                $photos = Arr::get($asanaUserData, 'photo');

                $asanaUser = AsanaUser::create([
                    'asana_id'      => Arr::get($asanaUserData, 'id'),
                    'token'         => Arr::get($authInformation, 'token'),
                    'refresh_token' => Arr::get($authInformation, 'refreshToken'),
                    'expire_at'     => date(\DateTime::ISO8601, time() + Arr::get($authInformation, 'expireAt')),
                    'photo'         => Arr::get((array)$photos, 'image_128x128'),
                ]);

                $user = new User([
                    'name'      => Arr::get($asanaUserData, 'name'),
                    'email'     => Arr::get($asanaUserData, 'email'),
                    'api_token' => uniqid('user', true),
                    'password'  => Hash::make(uniqid('user', true)),
                ]);

                $user->asanaUser()->associate($asanaUser);
                $user->save();
            });
        } else {
            $asanaUser->token         = Arr::get($authInformation, 'token');
            $asanaUser->refresh_token = Arr::get($authInformation, 'refreshToken');
            $asanaUser->expire_at     = date(\DateTime::ISO8601, time() + Arr::get($authInformation, 'expireAt'));
            $photos                   = Arr::get($asanaUserData, 'photo');
            $asanaUser->photo         = Arr::get((array)$photos, 'image_128x128');

            $asanaUser->user->update(['api_token' => uniqid('user', true)]);

            $asanaUser->save();
        }

        return $asanaUser->user;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }
}
