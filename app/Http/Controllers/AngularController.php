<?php

namespace App\Http\Controllers;

/**
 * Class AngularController
 *
 * @package App\Http\Controllers
 */
class AngularController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('angular/index');
    }
}