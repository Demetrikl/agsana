<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/oauth', 'Auth\AuthController@oauth');
Route::get('/login', 'Auth\AuthController@login');

Route::group(['middleware' => ['token.expiration', 'auth:api']], function () {
    Route::group(['prefix' => '/api/v1'], function () {
        Route::get('workspaces', 'AsanaController@workspaces');
        Route::get('workspaces/{workspaceId}/projects', 'AsanaController@projects');
        Route::get('workspaces/{workspaceId}/projects/{search}', 'AsanaController@projectsSearch');

        Route::get('projects/{projectId}', 'AsanaController@fullProject');
        Route::get('projects/{projectId}/tasks', 'AsanaController@tasks');
        Route::get('projects/{projectId}/sections', 'AsanaController@sections');

        Route::get('tasks/{taskId}', 'AsanaController@getTask');
        Route::put('tasks/{taskId}', 'AsanaController@updateTask');

        Route::get('user', 'AsanaController@user');
    });

    Route::get('/home', 'HomeController@index');

    // angular routes
    Route::any('/angular/{slug?}', 'AngularController@index')->where('slug', '(.*)');
});