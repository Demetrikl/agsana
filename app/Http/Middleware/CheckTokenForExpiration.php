<?php

namespace App\Http\Middleware;

use App\Services\AsanaService;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

/**
 * Class CheckTokenForExpiration
 *
 * @package App\Http\Middleware
 */
class CheckTokenForExpiration
{
    /**
     * @var AsanaService
     */
    protected $asanaService;

    /**
     * CheckTokenForExpiration constructor.
     *
     * @param AsanaService $asanaService
     */
    public function __construct(AsanaService $asanaService)
    {
        $this->asanaService = $asanaService;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     * @param  string|null              $guard
     *
     * @return mixed
     */
    public function handle($request, \Closure $next, $guard = null)
    {
        if (Auth::check() && !$this->asanaService->refreshToken()) {
            throw new UnauthorizedHttpException('Token has been expired');
        }

        return $next($request);
    }
}