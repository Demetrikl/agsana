<?php

namespace App\Http\Requests\Auth\AuthController;

use App\Http\Requests\Request;

/**
 * Class OauthRequest
 *
 * @package App\Http\Requests\Auth\AuthController
 */
class OauthRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'string|required',
            'state' => 'integer|required',
        ];
    }

    public function authorize()
    {
        return true;
    }
}