<?php

namespace App\Services;

use App\Models\User;
use Asana\Client;
use Illuminate\Support\Facades\Auth;

/**
 * Class AsanaService
 *
 * @package App\Services
 */
class AsanaService
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * AsanaService constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return bool
     */
    public function refreshToken()
    {
        if ($this->tokenHasBeenExpired()) {
            return false;
        }

        /** @var User $user */
        if ($user = Auth::user()) {
            $asanaUser = $user->asanaUser;

            $expireDate = new \DateTime($asanaUser->expire_at);
            $nowDate = new \DateTime();

            $diff = $nowDate->diff($expireDate);

            // skip if token is far from expiration (5mins)
            if ($diff->i > 5) {
                return true;
            }

            try {
                $this->client->dispatcher->refreshAccessToken();

                $asanaUser->token = $this->client->dispatcher->accessToken;
                $asanaUser->expire_at = date(\DateTime::ISO8601, time() + $this->client->dispatcher->expiresIn);

                $asanaUser->save();

                return true;
            } catch (\Exception $e) {
                return false;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function tokenHasBeenExpired()
    {
        /** @var User $user */
        if ($user = Auth::user()) {
            $asanaUser = $user->asanaUser;

            $expireDate = new \DateTime($asanaUser->expire_at);
            $nowDate = new \DateTime();

            $diff = $nowDate->diff($expireDate);

            return (bool)$diff->invert;
        }

        return true;
    }
}