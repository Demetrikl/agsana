<?php

namespace App\Providers;

use App\Models\User;
use App\Services\AsanaService;
use Asana\Client;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        $this->app->bind(AsanaService::class);

        // 3rd party libraries
        $this->app->bind(Client::class, function () {
            $credentials = [
                'client_id' => config('asana.client.id'),
                'client_secret' => config('asana.client.secret'),
                'redirect_uri' => config('asana.client.redirect_uri'),
            ];

            /** @var User $user */
            if ($user = Auth::user()) {
                $credentials['token'] = $user->asanaUser->token;
                $credentials['refresh_token'] = $user->asanaUser->refresh_token;
            }

            return Client::oauth($credentials);
        });
    }
}
